#!/bin/bash
ls /proc | sort -n | grep '[0-9]\+' >/vagrant/test.file
printf "PID\tTTY\t\tSTAT\tTIME\tCOMMAND\n"
for i in $(cat /vagrant/test.file);
        do
                ########PID##########
                printf "%s\t" $i
                ########TTY##########
                if test -z $(readlink /proc/$i/fd/0) #/bin/readlink /proc/$i/fd/0;
                then
                        printf "%s\t\t" "?"
                else
                        printf "%s\t" $(readlink /proc/$i/fd/0)
                fi
                ########STAT#########
                printf "%s\t" $(cat /proc/$i/stat | awk '{print $3}' 2>/dev/null)
                ########TIME#########
                utime=$(cat /proc/$i/stat | awk "{print $14}")  # stime 15
                cutime=$(cat /proc/$i/stat | awk "{print $16}") # cstime 17
                times=$(($utime+$cutime))

                printf "%s\t" $(calc $times/1000)
                #######COMMAND########
                if [[ $(cat /proc/$i/cmdline) > 0 ]]
                then
                        printf "%.60s\t" "$(tr \\0 ' ' < /proc/$i/cmdline)"
                else
                        printf "%s\t" $(cat /proc/$i/stat | awk '{print $2}' 2>/dev/null)
                fi

                #####################
                printf "\n"
        done