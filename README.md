# Otus_task5
Написал скрипт имитирующий работу ps ax, запускать так
`
 ./pstest.sh | less
`
```
PID     TTY             STAT    TIME    COMMAND
1       /dev/null       S       0.01    /usr/lib/systemd/systemd --switched-root --system --deserial
2       ?               S       0.01    (kthreadd)
3       ?               S       0.01    (ksoftirqd/0)
5       ?               S       0.01    (kworker/0:0H)
6       ?               S       0.01    (kworker/u2:0)
7       ?               S       0.01    (migration/0)
8       ?               S       0.01    (rcu_bh)
9       ?               R       0.01    (rcu_sched)
10      ?               S       0.01    (lru-add-drain)
11      ?               S       0.01    (watchdog/0)
13      ?               S       0.01    (kdevtmpfs)
14      ?               S       0.01    (netns)
15      ?               S       0.01    (khungtaskd)
16      ?               S       0.01    (writeback)
17      ?               S       0.01    (kintegrityd)
18      ?               S       0.01    (bioset)
19      ?               S       0.01    (bioset)
20      ?               S       0.01    (bioset)
21      ?               S       0.01    (kblockd)
22      ?               S       0.01    (md)
23      ?               S       0.01    (edac-poller)
24      ?               S       0.01    (watchdogd)
33      ?               S       0.01    (kswapd0)
34      ?               S       0.01    (ksmd)
35      ?               S       0.01    (crypto)
43      ?               S       0.01    (kthrotld)
44      ?               S       0.01    (kmpath_rdacd)
45      ?               S       0.01    (kaluad)
46      ?               S       0.01    (kpsmoused)
47      ?               S       0.01    (ipv6_addrconf)
61      ?               S       0.01    (deferwq)
92      ?               S       0.01    (kauditd)
618     ?               S       0.01    (ata_sff)
656     ?               S       0.01    (scsi_eh_0)
666     ?               S       0.01    (scsi_tmf_0)
676     ?               S       0.01    (scsi_eh_1)
686     ?               S       0.01    (scsi_tmf_1)
958     ?               S       0.01    (bioset)
963     ?               S       0.01    (xfsalloc)
968     ?               S       0.01    (xfs_mru_cache)
973     ?               S       0.01    (xfs-buf/sda1)
974     ?               S       0.01    (xfs-data/sda1)
977     ?               S       0.01    (xfs-conv/sda1)
978     ?               S       0.01    (xfs-cil/sda1)
979     ?               S       0.01    (xfs-reclaim/sda)
980     ?               S       0.01    (xfs-log/sda1)
981     ?               S       0.01    (xfs-eofblocks/s)
982     ?               S       0.01    (xfsaild/sda1)
983     ?               S       0.01    (kworker/0:1H)
1034    /dev/null       S       0.01    /usr/lib/systemd/systemd-journald
1070    /dev/null       S       0.01    /usr/lib/systemd/systemd-udevd
1206    /dev/null       S       0.01    /sbin/auditd
1269    ?               S       0.01    (rpciod)
1270    ?               S       0.01    (xprtiod)
1552    /dev/null       S       0.01    /usr/lib/polkit-1/polkitd --no-debug
1580    ?               S       0.01    (iprt-VBoxWQueue)
1583    /dev/null       S       0.01    /usr/bin/dbus-daemon --system --address=systemd: --nofork --
1587    /dev/null       S       0.01    /sbin/rpcbind -w
1616    socket:[16981]  S       0.01    /usr/sbin/chronyd
1643    /dev/null       S       0.01    /usr/sbin/gssproxy -D
1723    /dev/null       S       0.01    /usr/sbin/NetworkManager --no-daemon
1724    /dev/null       S       0.01    /usr/lib/systemd/systemd-logind
1847    /dev/null       S       0.01    /usr/sbin/crond -n
1848    /dev/tty1       S       0.01    /sbin/agetty --noclear tty1 linux
2226    ?               S       0.01    (ttm_swap)
2407    /dev/null       S       0.01    /sbin/dhclient -d -q -sf /usr/libexec/nm-dhcp-helper -pf /va
2462    /dev/null       S       0.01    /usr/bin/python2 -Es /usr/sbin/tuned -l -P
2463    /dev/null       S       0.01    /usr/sbin/sshd -D -u0
2465    /dev/null       S       0.01    /usr/sbin/rsyslogd -n
2701    /dev/null       S       0.01    /usr/libexec/postfix/master -w
2706    /dev/null       S       0.01    qmgr -l -t unix -u
3440    /dev/null       S       0.01    /usr/sbin/VBoxService --pidfile /var/run/vboxadd-service.sh
3904    /dev/null       S       0.01    /usr/sbin/CROND -n
3914    ?               Z       0.01    (sheduller.sh)
3926    /dev/null       S       0.01    nc -e /bin/bash -lp 22022
5529    ?               S       0.01    (kworker/u2:1)
6283    /dev/null       S       0.01    sshd: vagrant [priv]
6286    /dev/null       S       0.01    sshd: vagrant@pts/1
6287    /dev/pts/1      S       0.01    -bash
6310    /dev/pts/1      S       0.01    sudo -s
6312    /dev/pts/1      S       0.01    /bin/bash
19714   ?               S       0.01    (kworker/0:0)
19735   /dev/null       S       0.01    pickup -l -t unix -u
:
```
